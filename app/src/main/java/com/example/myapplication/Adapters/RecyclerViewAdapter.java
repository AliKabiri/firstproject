package com.example.myapplication.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {


    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        // now you need to get the view of the row:
        View view = layoutInflater.inflate(R.layout.item, viewGroup, false);
        // now you need to create an instance of ViewHOlder class that u created in this class;
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, int i) {

        viewHolder.textView.setText(String.valueOf(""));

    }

    @Override
    public int getItemCount() {
        return 12;
    }

    // you have to write this on your own, u have to create a class of viewholder that extends from
    //recycler view's view holder and pass it to the main class RecyclerView.Adapter<RecyclerAdapter.ViewHolder>

    // this is responsible for the rows and managing them

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView ;
        TextView textView;
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //itemView is now your row that u created.
            imageView = itemView.findViewById(R.id.itemImageView);
            textView = itemView.findViewById(R.id.itemTextView);
            checkBox = itemView.findViewById(R.id.itemCheckBox);

        }
    }
}
